import Button from "./components/Button/Button";
import "./App.scss";
import "./components/Button/Button.scss";
import "./components/Modal/Modal.scss";
import Modal from "./components/Modal/Modal";
import ModalWrapper from "./components/Modal/ModalWrapper";
import ModalHeader from "./components/Modal/ModalHeader";
import ModalClose from "./components/Modal/ModalClose";
import ModalBody from "./components/Modal/ModalBody";
import ModalFooter from "./components/Modal/ModalFooter";
import ModalImage from "./components/Modal/ModalImage";
import ModalText from "./components/Modal/ModalText";
import { useState } from "react";

function App() {
  const [isVisible, setIsVisible] = useState(false);
  const [modalType, setModalType] = useState(null);

  function changeModalState(type) {
    setIsVisible(!isVisible);
    setModalType(type);
  }

  function handleWrapperClick(e) {
    e.stopPropagation();
  }
  return (
    <>
      <div className="app-div">
        <Button
          type="button"
          onClick={() => changeModalState("delete")}
          className="new__button new__button--purple"
        >
          Open first modal
        </Button>
        <Button
          type="button"
          onClick={() => changeModalState("add")}
          className="new__button new__button--white"
        >
          Open second modal
        </Button>
      </div>
      {isVisible ? (
        <ModalWrapper className="modal__wrapper" onClick={changeModalState}>
          <Modal className="modal" onClick={handleWrapperClick}>
            <ModalHeader className="modal__header">
              <ModalClose
                className="modal__close-btn"
                onClick={changeModalState}
              ></ModalClose>
            </ModalHeader>
            <ModalBody className="modal__body">
              {modalType === "delete" ? (
                <>
                  <ModalImage className="modal__body-rectangle" />
                  <ModalText className="modal__body-title">
                    Product Delete!
                  </ModalText>
                  <ModalText className="modal__body-text">
                    By clicking the “Yes, Delete” button, PRODUCT NAME will be
                    deleted.
                  </ModalText>
                </>
              ) : (
                <>
                  <ModalText className="modal__body-title">
                    Add Product “NAME”
                  </ModalText>
                  <ModalText className="modal__body-text">
                    Description for you product
                  </ModalText>
                </>
              )}
            </ModalBody>
            {modalType === "delete" ? (
              <ModalFooter
                firstText="NO, CANCEL"
                secondaryText="YES, DELETE"
                firstClick={changeModalState}
                secondaryClick={changeModalState}
              ></ModalFooter>
            ) : (
              <ModalFooter
                firstText="ADD TO FAVORITE"
                firstClick={changeModalState}
              ></ModalFooter>
            )}
          </Modal>
        </ModalWrapper>
      ) : null}
    </>
  );
}

export default App;
