// import ModalWrapper from "./ModalWrapper";

function Modal({ className, onClick, children }) {
  return (
    <div onClick={onClick} className={className}>
      {children}
    </div>
  );
}

export default Modal;
