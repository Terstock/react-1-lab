function ModalText({ className, children }) {
  return <span className={className}>{children}</span>;
}

export default ModalText;
